import org.gradle.jvm.tasks.Jar

plugins {
	id("org.jetbrains.kotlin.jvm") version "1.9.24"
	id("org.openjfx.javafxplugin") version "0.1.0"
	id("application")
}

group = "imp-dev"
version = "1.0.0"
val jvm_version: String by project

repositories {
	mavenCentral()
}

dependencies {
	implementation(project(":impatient-javafx"))
	implementation(project(":impatient-json"))
	implementation(project(":impatient-logback"))
	implementation(project(":impatient-utils"))

	implementation("org.tomlj:tomlj:1.0+")

	testImplementation(project(":impatient-junit4"))
}

javafx {
	version = jvm_version
	modules("javafx.base", "javafx.graphics", "javafx.controls")
}

java.toolchain.languageVersion.set(JavaLanguageVersion.of(jvm_version.toInt()))

application.mainClass = "imp.lux.LuxMainKt"

tasks.withType<Jar> {
	manifest.attributes(mapOf(
		"Main-Class" to application.mainClass
	))

	// make a fat JAR - https://www.baeldung.com/kotlin/gradle-executable-jar
	duplicatesStrategy = DuplicatesStrategy.EXCLUDE
	val contents = configurations.runtimeClasspath.get()
		.map { if (it.isDirectory) it else zipTree(it) } +
		sourceSets.main.get().output
	from(contents)
}