# impatient-lux

This is a simple GUI for [Redshift](http://jonls.dk/redshift).
Redshift is a command-line program that adjusts the color temperature of your screen
(e.g. so you can turn it red at night and avoid problems falling asleep).
This program lets you set the color temperature and brightness manually,
and lets you define defaults that can be automatically applied on a custom schedule.

![screenshot](screenshot.png)

For the settings you're able to configure, see [lux-config.toml](src/main/resources/lux-config-default.toml)

## System Setup

Your system needs to have **redshift** installed.
(I don't know if the version matters; I've tested 1.12.)

You also need **Java 17** or higher installed, plus **JavaFX**.

## Usage Instructions

Put imp-lux.jar in a folder somewhere, then start it.
I can't tell you how to run a JAR file on your computer;
please try running `java -jar impatient-lux.jar` on the command line,
or try double-clicking on the file,
or search the internet if neither of those work.
(Warning: don't change the name of the JAR file.)

When the program runs, it will create 2 new files in its folder if they don't already exist:

* lux-config.toml contains useful options to change, and lots of explanation; please look at it.
 (After you change this file, you have to restart the program for your changes to take effect.)

* logback.xml can be used to make the program print or record more info as it runs.
 You can leave this alone unless you're trying to diagnose or report a bug.
 
 While the program is running, you can click the buttons to change the brightness and/or color temperature.
 The buttons at the bottom to tell the program to use the default brightness/color; the default varies by time of day and is defined in lux-config.toml.
 
 If you accidentally set the brightness too low and can't see anything, exit the program (alt + F4).
 The program will raise the brightness to 0.4 when it exits.

You can also use your keyboard. Use the arrow keys to navigate to different buttons,
and space or enter to activate the button you have selected (indicated by an underline).
You can also use the key `0` to reset brightness and color temperature to the current defaults,
and the key `1` to do basically the same thing except future changes to the defaults will be ignored.
(Just try it - it's hard to explain but easy to understand.)


## Developer Info

This program uses

* The **Kotlin** programming language.

* **Gradle** to build the project.

* **JavaFX** for the UI.

To build the JAR:

`./gradlew jar`

The jar will appear in `build/libs`.

To run the app without a jar, you CANNOT use `./gradlew run` like normal - there is some kind of module issue with JavaFX.
Instead, ask IntelliJ to run the main method in `imp.lux.LuxMainKt`.