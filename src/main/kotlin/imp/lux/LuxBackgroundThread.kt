package imp.lux

import imp.jfx.setLater
import imp.logback.shutdownLogbackAndExitError
import imp.lux.ui.AppState
import imp.util.logger
import javafx.beans.value.ChangeListener
import java.time.Duration
import kotlin.concurrent.thread


/**Background thread wakes up this often to decide what to do.*/
private val WAKE_MS = Duration.ofSeconds(10).toMillis()
/**Background thread tells the listener to reapply the current setting this often.*/
private val REAPPLY_MS = Duration.ofMinutes(5).toMillis()
/**If the thread is unable to run for this long, assume the computer is waking up from sleep and reapply.*/
private val SLEEP_DETECTION_MS = Duration.ofSeconds(30).toMillis()
/**If we detect the computer is waking up from sleep, reapply immediately, and reapply again after this much time.
 * (Sometimes the immediate reapply doesn't work, or is immediately undone as the computer wakes up.)*/
private val REAPPLY_POST_SLEEP_MS = Duration.ofSeconds(5).toMillis()

private val log = LuxBackgroundThread::class.logger


/**A background thread that tells AppState to reapply the current brightness and temperature periodically.
 * (Some events can undo Redshift, such as adding or removing a monitor.)
 * Also notices when the defaults change, and applies the new defaults if the settings say to use them.
 * As this thread starts up, it applies a brightness and color value immediately, so you don't need to.
 * The constructor must be called on the JavaFX main thread.*/
class LuxBackgroundThread (
	private val state: AppState,
	private val config: LuxConfig,
	initialSettings: LiveSettings,
) {
	private val sync = Object()
	private var settings = initialSettings
	private var currentBrightness: Float
	private var currentTemperatureK: Int
	/**The next time this thread should reapply the current values (whatever they are), in milliseconds.*/
	private var nextApply: Long = Long.MIN_VALUE

	init {
		state.currentBrightness.addListener(ChangeListener {_, _, b -> onChange(b.toFloat(), state.currentTemperatureK.get())})
		state.currentTemperatureK.addListener(ChangeListener {_, _, t -> onChange(state.currentBrightness.get(), t.toInt())})
		state.settings.addListener(ChangeListener {_, _, s -> onChangeSettings(s)})
	}

	/**Call this when someone else invokes Redshift, so this thread knows what the screen is currently displaying.*/
	private fun onChange(brightness: Float, temperatureK: Int) {
		synchronized(sync) {
			this.currentBrightness = brightness
			this.currentTemperatureK = temperatureK
		}
	}

	/**Call this when the settings change, from any thread.*/
	private fun onChangeSettings(settings: LiveSettings) {
		synchronized(sync) {
			log.debug("Notified about new {}.", settings)
			this.settings = settings
			sync.notifyAll()
		}
	}


	init {
		currentBrightness = settings.brightness ?: config.defaultBrightness()
		currentTemperatureK = settings.temperatureK ?: config.defaultColorTemperatureK()
		log.debug("Starting background thread with initial brightness $currentBrightness, temperature ${currentTemperatureK}K")

		thread(isDaemon = false, name = "background") {
			try {
				runInBackground()
			} catch(e: Throwable) {
				log.error("Error in background thread.", e)
				shutdownLogbackAndExitError()
			}
		}
	}


	private fun runInBackground() {
		synchronized(sync) {
			log.trace("Background thread recheck.")
			var currentDefault = config.default()
			nextApply = Long.MIN_VALUE //force a reapply immediately
			var sleepDetection = Long.MAX_VALUE

			while (true) {
				if(!state.backgroundThreadShouldContinue) {
					log.debug("Background thread stopping as requested.")
					return
				}

				val now = System.currentTimeMillis()
				var apply = false
				var sleepWasDetected = false
				if(now > sleepDetection) {
					log.info("Computer appears to be waking up from sleep; will reapply.")
					apply = true
					sleepWasDetected = true
				} else if (now > nextApply) {
					log.debug("Time to reapply (periodic).")
					apply = true
				} else if(config.default() != currentDefault) {
					log.debug("Applying default brightness and/or temperature.")
					apply = true
					currentDefault = config.default()
					state.currentDefault.setLater(currentDefault)
				}

				if (apply) {
					val b = settings.brightness ?: currentDefault.brightness
					val t = settings.temperatureK ?: currentDefault.colorTemperatureK
					state.applyRedshiftLater(b, t)
					currentBrightness = b
					currentTemperatureK = t
					nextApply = now + if(sleepWasDetected) REAPPLY_POST_SLEEP_MS else REAPPLY_MS
				}

				sleepDetection = now + SLEEP_DETECTION_MS
				sync.wait(if(sleepWasDetected) REAPPLY_POST_SLEEP_MS else WAKE_MS)
			}
		}
	}
}