package imp.lux

import imp.util.availableToString
import imp.util.kdebug
import imp.util.logger
import java.util.concurrent.TimeUnit

/**Generates and executes Redshift commands.*/
object Redshift {
	private val log = Redshift::class.logger

	/**Returns a description of a problem with the arguments, or null if they're ok. See redshiftScreen.
	 * This does not actually execute the redshift program and will return quickly.*/
	fun getProblem(brightness: Float, temperatureK: Int): String? {
		if (brightness < 0.1 || brightness > 1)
			return "Brightness must be between 0.1 and 1, not $brightness"
		if (temperatureK < 1000 || temperatureK > 25000)
			return "Color temperature must be between 1000K and 25000K, not ${temperatureK}K"
		return null
	}


	/**Changes the screen brightness and color temperature using redshift.
	 * @param brightness 0-1
	 * @param temperatureK in thousands: 6500 is neutral; lower is redder (down to 1000); higher is bluer (up to 25000)*/
	fun redshiftScreen(brightness: Float, temperatureK: Int, config: LuxConfig) {
		log.info("Redshifting screen to brightness {} and color temperature {}K.", brightness, temperatureK)
		val problem = getProblem(brightness, temperatureK)
		if (problem != null)
			throw RuntimeException(problem)

		val args = listOf(config.redshiftBinLocation, "-orP", "-O", "${temperatureK}K", "-b", "$brightness")
		//-o: one-shot; -r: disable temperature transitions (do it quickly, then exit); P: reset any previous changes

		log.kdebug {"Executing ${args.joinToString(" ")}"}
		val proc = ProcessBuilder(args).start()
		val finished = proc.waitFor(10, TimeUnit.SECONDS)
		val outStr = proc.inputStream.availableToString()
		val errStr = proc.errorStream.availableToString()
		if(!outStr.isEmpty())
			log.debug("Output: {}", outStr.trim())
		if(!errStr.isEmpty())
			log.debug("Error output: {}", errStr.trim())

		if (!finished) {
			proc.destroyForcibly()
			throw Exception("Process failed to terminate promptly.")
		}
		log.debug("Finished successfully.")
	}
}