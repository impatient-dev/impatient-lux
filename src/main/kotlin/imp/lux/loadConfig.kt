package imp.lux

import imp.util.ClassResources
import imp.util.logger
import org.apache.commons.io.FileUtils
import org.tomlj.Toml
import org.tomlj.TomlArray
import org.tomlj.TomlParseResult
import org.tomlj.TomlTable
import java.io.FileInputStream
import java.io.InputStream
import java.nio.file.Files
import java.nio.file.Paths

private val log = LuxConfig::class.logger

const val LUX_DEFAULT_CONFIG_PATH = "/lux-config-default.toml"

fun loadLuxConfig() : LuxConfig { //TODO parsing unit test
	//locate the file
	val configFile = Paths.get("lux-config.toml")
	log.debug("Parsing config file {}.", configFile)
	if (!Files.exists(configFile)) {
		log.info("Config file missing; creating.")
		ClassResources.open(LuxConfig::class, LUX_DEFAULT_CONFIG_PATH).use { ins ->
			FileUtils.copyToFile(ins, configFile.toFile())
		}
	}

	FileInputStream(configFile.toFile()).use { ins ->
		return parseConfig(ins)
	}
}


fun parseConfig(ins: InputStream): LuxConfig {
	val data = Toml.parse(ins)
	if(data.hasErrors()) {
		for(error in data.errors())
			log.error("Error in config file at {}: {}", error.position(), error.message)
		throw Exception("There was a problem with the config file, at ${data.errors()[0].position()}: ${data.errors()[0].message}")
	}

	val binLocation = data["redshiftLocation"]
	if(binLocation == null || binLocation !is String)
		throw Exception("redshiftLocation must be a string")
	log.debug("Redshift binary: {}", binLocation)

	//read brightness options
	val inBright = requireArray(data, "brightnessOptions")
	val outBright = ArrayList<Float>(inBright.size())
	for(value in inBright.toList())
		outBright.add(requireBrightness(value))
	log.debug("Brightness options: $outBright")

	//read color options
	val inColor = requireArray(data, "temperatureOptionsK")
	val outColor = ArrayList<Int>(inColor.size())
	for(value in inColor.toList())
		outColor.add(requireColorTemperature(value))
	log.debug("Color options: $outColor")

	//read defaults
	val inDefaults = data["default"]
	val outDefaults = ArrayList<LuxDefault>()
	if(inDefaults != null) {
		if(inDefaults !is TomlArray)
			throw Exception("'default' must be an array.")
		for(default in inDefaults.toList()) {
			if(default !is TomlTable)
				throw Exception("Every default must be a table of values.")
			val time = requireHourMinute(default["start"] ?: throw Exception("Every default requires a start time."))
			val brightness = requireBrightness(default["brightness"] ?: throw Exception("Every default requires a brightness."))
			val temperature = requireColorTemperature(default["temperatureK"] ?: throw Exception("Every default requires a color temperature."))
			outDefaults.add(LuxDefault(time, brightness, temperature))
		}
	}

	return LuxConfig(binLocation, outBright, outColor, outDefaults)
}



private fun requireArray(data: TomlParseResult, path: String): TomlArray {
	val out = data[path]
	if(out is TomlArray)
		return out
	throw Exception("Expected an array for $path")
}

private fun requireBrightness(value: Any): Float {
	val f: Float = when(value) {
		is Float -> value
		is Double -> value.toFloat()
		is Int -> value.toFloat()
		is Long -> value.toFloat()
		else -> throw Exception("Brightness must be a number, not '$value'.")
	}
	val problem = Redshift.getProblem(f, 6500)
	if(problem != null)
		throw Exception(problem)
	return f
}


private fun requireColorTemperature(value: Any): Int {
	val i = when(value) {
		is Int -> value
		is Long -> colorLongToInt(value)
		else -> throw Exception("Color temperature must be a whole number: $value")
	}
	val problem = Redshift.getProblem(1f, i)
	if(problem != null)
		throw Exception(problem)
	return i
}

private fun colorLongToInt(value: Long): Int {
	try {
		return Math.toIntExact(value)
	} catch(e: ArithmeticException) {
		throw Exception("Color temperature is too large/small: $value")
	}
}


private fun requireHourMinute(value: Any): HourMinute {
	try {
		if (value !is String)
			throw Exception()

		val str = value.toLowerCase()
		var addHour: Int = 0
		val hmStr: String = when {
			str.endsWith(" am") -> str.substring(0, str.length - 3)
			str.endsWith("am") -> str.substring(0, str.length - 2)
			str.endsWith(" pm") -> {addHour = 12; str.substring(0, str.length - 3)}
			str.endsWith("pm") -> {addHour = 12; str.substring(0, str.length - 2)}
			else -> str
		}

		val parts = hmStr.split(":")
		if (parts.size != 2)
			throw Exception()
		return HourMinute(Integer.parseInt(parts[0]) + addHour, Integer.parseInt(parts[1]))
	} catch(e: Exception) {
		log.debug("Failed to parse $value as hour:minute.", e)
		throw Exception("Required an hour:minute string, not $value")
	}
}