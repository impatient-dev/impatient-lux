package imp.lux

import imp.lux.LuxDefault.Companion.DEFAULT

/**Loaded from our main config file. Does not change while the app is running.*/
class LuxConfig (
	/**Path to the executable.*/
	val redshiftBinLocation: String,
	val brightnessOptions: List<Float>,
	val temperatureKOptions: List<Int>,
	val defaults: List<LuxDefault>
) {
	fun default(time: HourMinute = HourMinute.now()): LuxDefault {
		if(defaults.isEmpty())
			return DEFAULT
		for(i in 0 until defaults.size) {
			if(time < defaults[i].start)
				return if(i == 0) defaults.last() else defaults[i - 1]
		}
		return defaults.last()
	}

	fun defaultBrightness(time: HourMinute = HourMinute.now()): Float = default(time).brightness
	fun defaultColorTemperatureK(time: HourMinute = HourMinute.now()): Int = default(time).colorTemperatureK
}

data class LuxDefault (
	val start: HourMinute,
	val brightness: Float,
	val colorTemperatureK: Int
) {
	companion object {
		/**If the user doesn't supply any defaults, use this one.*/
		val DEFAULT = LuxDefault(HourMinute(0, 0), 1f, 6500)
	}
}
