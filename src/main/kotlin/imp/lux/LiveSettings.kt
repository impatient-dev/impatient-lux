package imp.lux

import com.fasterxml.jackson.module.kotlin.readValue
import imp.json.impJackson
import imp.util.logger
import java.nio.file.Files
import java.nio.file.Paths

private val log = LiveSettings::class.logger

/**Settings that can be changed while the program is running.
 * These settings are persisted as a JSON object.
 * For both values, null means "no specific choice made - use the default".*/
data class LiveSettings (
	val brightness: Float? = null,
	val temperatureK: Int? = null
) {
	override fun toString() = "Settings(brightness=$brightness, temperature=$temperatureK K)"
}


private val jsonMapper = impJackson()

private val settingsFile = Paths.get("lux-settings.json")

fun loadLuxSettings() : LiveSettings {
	val file = settingsFile
	if(!Files.exists(file)) {
		log.info("Settings file {} missing - using defaults.", file)
		return LiveSettings()
	}

	try {
		log.info("Parsing settings file {}", file)
		return jsonMapper.readValue(file.toFile())
	} catch(e: Exception) {
		log.error("Invalid settings file.", e)
		return LiveSettings()
	}
}


fun saveLuxSettings(settings: LiveSettings) {
	val file = settingsFile
	log.info("Saving {} to {}", settings, file)
	Files.deleteIfExists(file)
	jsonMapper.writeValue(file.toFile(), settings)
}