package imp.lux

import java.time.LocalTime

/**Represents a time of day in 24-hour time.*/
class HourMinute (
	val hour: Int,
	val minute: Int
) : Comparable<HourMinute> {
	init {
		if(hour < 0 || hour > 23)
			throw IllegalArgumentException("Invalid hour: $hour")
		if(minute < 0 || minute > 59)
			throw IllegalArgumentException("Invalid minute: $minute")
	}

	override fun compareTo(that: HourMinute): Int {
		var out = this.hour.compareTo(that.hour)
		if(out != 0)
			return out
		return this.minute.compareTo(that.minute)
	}

	override fun toString() = "$hour:$minute"

	companion object {
		fun now(): HourMinute {
			val time = LocalTime.now()
			return HourMinute(time.hour, time.minute)
		}
	}
}

