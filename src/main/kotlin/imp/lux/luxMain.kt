package imp.lux

import com.sun.javafx.application.LauncherImpl
import imp.logback.initLogbackCreateConfigIfMissing
import imp.logback.shutdownLogbackAndExitError
import imp.lux.ui.LuxJfxApp
import imp.lux.ui.LuxJfxPreloader
import imp.util.assertWorkingDirContains
import imp.util.logger
import java.nio.file.Paths

/**Used to get references to classpath stuff.*/
private class LuxApp


fun main() {
	assertWorkingDirContains("src/main/kotlin/imp/lux", "imp-lux.jar")
	initLogbackCreateConfigIfMissing(Paths.get("logback.xml"), "/logback-default.xml", LuxApp::class)

	val log = LuxApp::class.logger
	log.info("Starting up.")

	try {
		LauncherImpl.launchApplication(LuxJfxApp::class.java, LuxJfxPreloader::class.java, emptyArray())
	} catch(e: Throwable) {
		log.error("Application failed to start.", e)
		shutdownLogbackAndExitError()
	}
}