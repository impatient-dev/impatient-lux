package imp.lux.ui

import imp.jfx.JfxUtil
import imp.logback.shutdownLogbackAndExitError
import imp.lux.LuxBackgroundThread
import imp.lux.loadLuxConfig
import imp.lux.loadLuxSettings
import imp.util.logger
import javafx.application.Application
import javafx.application.Preloader
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.stage.Stage

private val appTitle = "impatient-lux"

/**Sets the application name that appears in the Gnome dock. Otherwise, it's a class name. See stackoverflow.com/a/54467323.*/
class LuxJfxPreloader : Preloader() {
	override fun start(primaryStage: Stage?) {
		com.sun.glass.ui.Application.GetApplication().name = appTitle
	}
}


/**Launches the main window. Shuts down the JVM if it fails.*/
class LuxJfxApp : Application() {
	private val log = LuxJfxApp::class.logger

	override fun start(primaryStage: Stage) {
		try {
			val config = loadLuxConfig()
			val settings = loadLuxSettings()
			log.debug("Launching UI.")
			val state = AppState(config, settings)

			primaryStage.title = appTitle
			primaryStage.icons.add(JfxUtil.loadImage(LuxJfxApp::class, "/impatient-lux-icon.png"))
			val window = LuxWindow(state, config)
			primaryStage.scene = Scene(window.root)
			LuxBackgroundThread(state, config, settings)
			primaryStage.scene.window.onCloseRequest = EventHandler { window.onExit() }
			primaryStage.scene.onKeyPressed = EventHandler { ev -> window.onKey(ev.code)}
			primaryStage.sizeToScene()
			primaryStage.show()
			log.debug("UI startup succeeded.")
		} catch(e: Exception) {
			log.error("UI startup failed.", e)
			shutdownLogbackAndExitError()
		}
	}
}