package imp.lux.ui

import imp.jfx.addStylesheetResource
import imp.jfx.jfxBind
import imp.jfx.withStyle
import imp.logback.shutdownLogbackAndExitNormally
import imp.lux.LiveSettings
import imp.lux.LuxConfig
import imp.lux.Redshift
import imp.lux.saveLuxSettings
import imp.util.addIfMissing
import imp.util.logger
import javafx.beans.value.ChangeListener
import javafx.event.EventHandler
import javafx.geometry.HPos
import javafx.geometry.Pos
import javafx.scene.Parent
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.input.KeyCode
import javafx.scene.layout.ColumnConstraints
import javafx.scene.layout.GridPane
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox

/**CSS class name applied to the button representing the brightness/color that is currently applied.*/
private val ACTIVE = "active"
/**On exit, raise the brightness to this.*/
private val EXIT_MIN_BRIGHTNESS = 0.4f

/**The main window of this application.
 * This window expects to be created before the background thread, so certain things are updated immediately.
 * Since this window doesn't have access to its Stage or Window, you have to set up a listener to call onExit() and onKey() yourself.*/
class LuxWindow (
	private val state: AppState,
	config: LuxConfig
) {
	private val log = LuxWindow::class.logger

	private val vGrid = GridPane()

	private val vBrightnessTitle = Label("Brightness").withStyle("colTitle")
	private val vTemperatureTitle = Label("Color Temp.").withStyle("colTitle")
	private val vBrightnessCol = VBox().also {it.alignment = Pos.TOP_CENTER}
	private val vTemperatureCol = VBox().also {it.alignment = Pos.TOP_CENTER}
	
	private val brightnessButtons = HashMap<Float, Button>()
	private val temperatureButtons = HashMap<Int, Button>()
	private var vCurrentBrightness: Button? = null
	private var vCurrentTemperature: Button? = null

	private val vDefaultTitle = Label("Default").withStyle("colTitle")
	private val vDefaultBrightness = Button().also {it.onAction = EventHandler {onBtnDefaultBrightness()}}
	private val vDefaultTemperature = Button().also {it.onAction = EventHandler {onBtnDefaultTemperature()}}

	val root: Parent = VBox(vGrid).also {it.addStylesheetResource("/style.css", LuxWindow::class)}

	init {
		for(b in config.brightnessOptions) {
			val btn = Button("$b").withStyle("option")
			btn.onAction = EventHandler { onBtnBrightness(b) }
			brightnessButtons[b] = btn
			vBrightnessCol.children.add(btn)
		}
		for(t in config.temperatureKOptions) {
			val btn = Button("$t K").withStyle("option")
			btn.onAction = EventHandler { onBtnTemperature(t) }
			temperatureButtons[t] = btn
			vTemperatureCol.children.add(btn)
		}

		val colSpec = ColumnConstraints(50.0, 100.0, 200.0, Priority.SOMETIMES, HPos.CENTER, true)
		var r = 0
		vGrid.columnConstraints.add(colSpec)
		vGrid.columnConstraints.add(colSpec)
		vGrid.addRow(r++, vBrightnessTitle, vTemperatureTitle)
		vGrid.addRow(r++, vBrightnessCol, vTemperatureCol)
		vGrid.add(vDefaultTitle, 0, r++, 2, 1)
		vGrid.addRow(r++, vDefaultBrightness, vDefaultTemperature)
		
		state.currentBrightness.addListener(ChangeListener { _, _, b -> onChangeBrightness(b.toFloat())})
		state.currentTemperatureK.addListener(ChangeListener { _, _, t -> onChangeTemperature(t.toInt())})
		state.settings.addListener(ChangeListener { _, _, s -> onChangeSettings(s)})
		vDefaultBrightness.textProperty().jfxBind(state.currentDefault) { "${it.brightness}" }
		vDefaultTemperature.textProperty().jfxBind(state.currentDefault) { "${it.colorTemperatureK} K" }

		onChangeSettings(state.settings.value)
	}

	
	private fun onChangeBrightness(brightness: Float) {
		if(state.settings.value.brightness == null)
			return
		log.trace("Updating active buttons after brightness change to {}.", brightness)
		vCurrentBrightness?.styleClass?.remove(ACTIVE)
		vCurrentBrightness = brightnessButtons[brightness]
		vCurrentBrightness?.styleClass?.addIfMissing(ACTIVE)
	}
	
	private fun onChangeTemperature(temperatureK: Int) {
		if(state.settings.value.temperatureK == null)
			return
		log.trace("Updating active buttons after color temperature change to {}.", temperatureK)
		vCurrentTemperature?.styleClass?.remove(ACTIVE)
		vCurrentTemperature = temperatureButtons[temperatureK]
		vCurrentTemperature?.styleClass?.addIfMissing(ACTIVE)
	}
	
	private fun onChangeSettings(settings: LiveSettings) {
		log.info("Updating UI after settings changed to {}.", settings)

		if(settings.brightness == null) {
			vDefaultBrightness.styleClass.addIfMissing(ACTIVE)
			vCurrentBrightness?.styleClass?.remove(ACTIVE)
			vCurrentBrightness = null
		} else {
			vDefaultBrightness.styleClass.remove(ACTIVE)
			if(vCurrentBrightness == null)
				vCurrentBrightness = brightnessButtons[state.currentBrightness.get()].also {it?.styleClass?.addIfMissing(ACTIVE)}
		}
		
		if(settings.temperatureK == null) {
			vDefaultTemperature.styleClass.addIfMissing(ACTIVE)
			vCurrentTemperature?.styleClass?.remove(ACTIVE)
			vCurrentTemperature = null
		} else {
			vDefaultTemperature.styleClass.remove(ACTIVE)
			if(vCurrentTemperature == null)
				vCurrentTemperature = temperatureButtons[state.currentTemperatureK.get()].also {it?.styleClass?.addIfMissing(ACTIVE)}
		}
	}


	private fun onBtnBrightness(brightness: Float) {
		log.info("User requested new brightness: {}.", brightness)
		state.settings.value = state.settings.value.copy(brightness = brightness)
		state.applyRedshiftLater(brightness, state.currentTemperatureK.get())
	}

	private fun onBtnTemperature(temperatureK: Int) {
		log.info("User requested new temperature: {}K.", temperatureK)
		state.settings.value = state.settings.value.copy(temperatureK = temperatureK)
		state.applyRedshiftLater(state.currentBrightness.get(), temperatureK)
	}

	private fun onBtnDefaultBrightness() {
		if(state.settings.value.brightness != null)
			state.settings.value = state.settings.value.copy(brightness = null)
		//refresh or apply desired setting
		state.applyRedshiftLater(state.currentDefault.value.brightness, state.currentTemperatureK.get())
	}

	private fun onBtnDefaultTemperature() {
		if(state.settings.value.temperatureK != null)
			state.settings.value = state.settings.value.copy(temperatureK = null)
		//refresh or apply desired setting
		state.applyRedshiftLater(state.currentBrightness.get(), state.currentDefault.value.colorTemperatureK)
	}

	private fun onBtnAllDefaults() {
		log.info("User requested all defaults be applied.")
		state.settings.value = LiveSettings(null, null)
		val d = state.currentDefault.value
		state.applyRedshiftLater(d.brightness, d.colorTemperatureK)
	}

	private fun onBtnDefaultsApplyAndFreeze() {
		val d = state.currentDefault.value
		log.info("User requested applying the current default and preventing it from changing: {}", d)
		state.settings.value = LiveSettings(d.brightness, d.colorTemperatureK)
		state.applyRedshiftLater(d.brightness, d.colorTemperatureK)
	}

	/**Must be called by code outside this class when a key is pressed.*/
	fun onKey(key: KeyCode) {
		when(key) {
			KeyCode.DIGIT0, KeyCode.NUMPAD0 -> onBtnAllDefaults()
			KeyCode.DIGIT1, KeyCode.NUMPAD1 -> onBtnDefaultsApplyAndFreeze()
			else -> Unit
		}
	}


	/**Must be called by code outside this class when the user requests the window to close.*/
	fun onExit() {
		log.info("Hiding window and saving settings.")
		state.backgroundThreadShouldContinue = false
		root.scene.window.hide()
		try {
			saveLuxSettings(state.settings.value)
		} catch(e: Exception) {
			log.error("Failed to save settings.", e)
		}

		if(state.currentBrightness.get() < EXIT_MIN_BRIGHTNESS) {
			log.info("Exiting with low brightness. Raising from {} to {}.", state.currentBrightness.get(), EXIT_MIN_BRIGHTNESS)
			Redshift.redshiftScreen(EXIT_MIN_BRIGHTNESS, state.currentTemperatureK.get(), state.config)
		}

		shutdownLogbackAndExitNormally()
	}
}