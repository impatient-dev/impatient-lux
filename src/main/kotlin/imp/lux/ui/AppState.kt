package imp.lux.ui

import imp.lux.LiveSettings
import imp.lux.LuxConfig
import imp.lux.Redshift
import javafx.application.Platform
import javafx.beans.property.SimpleFloatProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import java.util.concurrent.Executors
import java.util.concurrent.ThreadFactory


/**A set of properties that represent the current application state. (The parts the UI cares about anyways.)
 * This class is how the UI interacts with the background thread.*/
class AppState (
	val config: LuxConfig,
	initialSettings: LiveSettings
) {
	val settings = SimpleObjectProperty(initialSettings)
	/**The current applied brightness, which may or may not be a default.*/
	val currentBrightness = SimpleFloatProperty(Float.NaN)
	/**The current applied color temperature, which may or may not be a default.*/
	val currentTemperatureK = SimpleIntegerProperty(-1)
	val currentDefault = SimpleObjectProperty(config.default())
	/**Set this to false to ask the background thread to exit.*/
	@Volatile var backgroundThreadShouldContinue = true

	private val redshiftThread = Executors.newSingleThreadExecutor(ThreadFactory { runnable ->
		Thread(runnable, "redshift").also {it.isDaemon = true}
	})

	/**Executes this command in the Redshift thread, and concurrently changes the current brightness and temperature properties.*/
	fun applyRedshiftLater(brightness: Float, temperatureK: Int) {
		redshiftThread.submit {Redshift.redshiftScreen(brightness, temperatureK, config)}
		Platform.runLater {
			currentBrightness.set(brightness)
			currentTemperatureK.set(temperatureK)
		}
	}
}